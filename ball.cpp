#include "ball.h"
#include "main.h"

Ball::Ball(float x, float y, float s, color_t color) {
    this->position = glm::vec3(x, y, 0);
    this->rotation = 0;
    speed = s;
    // Yvelovity = 0 ; 
    // gravity = 0.005 ;
    // Our vertices. Three consecutive floats give a 3D vertex; Three consecutive vertices give a triangle.
    // A cube has 6 faces with 2 triangles each, so this makes 6*2=12 triangles, and 12*3 vertices
    static const GLfloat vertex_buffer_data[] = {
        0.2, -0.1, 0.0,
        0.2, -0.7, 0.0,
        0.5, -0.7, 0.0,
        0.5, -0.7, 0.0,
        0.5, -0.1, 0.0,
        0.2, -0.1, 0.0,
    } ; 

    static const GLfloat vertex_buffer_data_jetpack[] = {
        0.2, -0.2, 0.0,
        0.2, -0.6, 0.0,
        0.0, -0.6, 0.0,
        0.0, -0.6, 0.0,
        0.0, -0.2, 0.0,
        0.2, -0.2, 0.0,
    } ; 

    static const GLfloat vertex_buffer_data_legs[] ={
        0.4f, -0.7f, -0.0f, //leg 1
        0.2f, -0.7f, 0.0f,
        0.3f, -1.0f,0.0f, 
        0.5f, -0.7f, 0.0f, //leg 2
        0.3f, -0.7f, 0.0f,
        0.4f, -1.0f, 0.0f, 
    } ; 


        //  0.25f,-0.4f,-0.0f, // triangle 1 : begin
        // -0.0f,-0.1f, 0.0f,
        //  0.5f,-0.1f, 0.0f, // triangle 1 : end
        //  0.25f, -0.4f,-0.0f, // triangle 2 : begin
        // -0.0f,-0.7f,-0.0f,
        // 0.5f, -0.7f, -0.0f, // triangle 2 : end 
        // 0.0f, -0.7f, -0.0f, //leg 1
        // 0.2f, -0.7f, 0.0f,
        // 0.1f, -1.0f,0.0f, 
        // 0.5f, -0.7f, 0.0f, //leg 2
        // 0.3f, -0.7f, 0.0f,
        // 0.4f, -1.0f, 0.0f, 


    static const GLfloat vertex_buffer_data_hair[] = {
        0.400, 0.225, 0.0,
        0.125, 0.225, 0.0,
        0.125, 0.0, 0.0 ,
    } ; 

    static GLfloat vertex_buffer_data_eye[190];
    int nos = 6 ; 
    float rad = 0.03 ; 
    for(int i = 0 ; i < nos ; i++)
		{
				vertex_buffer_data_eye[9*i] = 0.325f ; 
				vertex_buffer_data_eye[9*i+1] = 0.095f ; 
				vertex_buffer_data_eye[9*i+2] = 0.0f ; 
				vertex_buffer_data_eye[9*i+3] = rad * cos(2*M_PI*(i)/nos)+0.325 ; 
				vertex_buffer_data_eye[9*i+4] = rad * sin(2*M_PI*(i)/nos) +0.095;
				vertex_buffer_data_eye[9*i+5] = 0.0f ;
				vertex_buffer_data_eye[9*i+6] = rad * cos(2*M_PI*(i+1)/nos) + 0.325; 
				vertex_buffer_data_eye[9*i+7] = rad * sin(2*M_PI*(i+1)/nos) + 0.095;
				vertex_buffer_data_eye[9*i+8] = 0.0f ;
	    }

    static GLfloat vertex_buffer_data_head[190] ; 
    int numberOfSides = 10 ; 
    float radius = 0.15 ; 
    for(int i = 0 ; i < numberOfSides ; i++)
		{
				vertex_buffer_data_head[9*i] = 0.275f ; 
				vertex_buffer_data_head[9*i+1] = 0.075f ; 
				vertex_buffer_data_head[9*i+2] = 0.0f ; 
				vertex_buffer_data_head[9*i+3] = radius * cos(2*M_PI*(i)/numberOfSides)+0.275 ; 
				vertex_buffer_data_head[9*i+4] = radius * sin(2*M_PI*(i)/numberOfSides) +0.075;
				vertex_buffer_data_head[9*i+5] = 0.0f ;
				vertex_buffer_data_head[9*i+6] = radius * cos(2*M_PI*(i+1)/numberOfSides) + 0.275; 
				vertex_buffer_data_head[9*i+7] = radius * sin(2*M_PI*(i+1)/numberOfSides) + 0.075;
				vertex_buffer_data_head[9*i+8] = 0.0f ;
	    }


    this->object_head = create3DObject(GL_TRIANGLES, numberOfSides*3, vertex_buffer_data_head, COLOR_CREAM, GL_FILL);
    this->object_hair = create3DObject(GL_TRIANGLES, 1*3, vertex_buffer_data_hair, COLOR_BLACK, GL_FILL);
    this->object_body = create3DObject(GL_TRIANGLES, 2*3, vertex_buffer_data, color, GL_FILL);
    this->object_legs = create3DObject(GL_TRIANGLES, 2*3, vertex_buffer_data_legs, COLOR_NAVY, GL_FILL);
    this->object_jetpack = create3DObject(GL_TRIANGLES, 2*3, vertex_buffer_data_jetpack, COLOR_CRIMSON) ;
    this->object_eye =  create3DObject(GL_TRIANGLES, nos*3, vertex_buffer_data_eye, COLOR_BLACK) ;
}

void Ball::draw(glm::mat4 VP) {
    Matrices.model = glm::mat4(1.0f);
    glm::mat4 translate = glm::translate (this->position);    // glTranslatef
    glm::mat4 rotate    = glm::rotate((float) (this->rotation * M_PI / 180.0f), glm::vec3(1, 0, 0));
    // No need as coords centered at 0, 0, 0 of cube arouund which we waant to rotate
    // rotate          = rotate * glm::translate(glm::vec3(0, -0.6, 0));
    Matrices.model *= (translate * rotate);
    glm::mat4 MVP = VP * Matrices.model;
    glUniformMatrix4fv(Matrices.MatrixID, 1, GL_FALSE, &MVP[0][0]);
    draw3DObject(this->object_head) ;
    draw3DObject(this->object_body);
    draw3DObject(this->object_legs) ; 
    draw3DObject(this->object_jetpack) ; 
    draw3DObject(this->object_hair) ; 
    draw3DObject(this->object_eye) ; 
}

void Ball::set_position(float x, float y) {
    this->position = glm::vec3(x, y, 0);
}

void Ball::left(float l){
    this->position.x -= l  ; 
}

void Ball::right(float r){
    this->position.x += r ; 
}

void Ball::jump(float v){
    this->set_yvelocity(v) ; 
}

void Ball::up(float v){
    this->Yvelocity += 0.021 ; 
}

void Ball::down(float v){
    this->Yvelocity -= 0.01 ; 
}

void Ball::set_speed(float s){
    this->speed = s ; 
}

void Ball::set_yvelocity(float yv){
    this->Yvelocity = yv ; 
}

float Ball::return_x(){
    return this->position.x ; 
}

float Ball::return_y(){
    return this->position.y ; 
}

int Ball::magnet_pull_horizontal(bounding_box_t m){
    
    if (this->position.x != m.x )
    {
        if(this->position.x < m.x)
        {
            return 1 ; 
        }
        else {
            return 2 ; 
        }
    }

}

int Ball::magnet_pull_vertical(bounding_box_t m){
    
    if (this->position.y != m.y )
    {
        if(this->position.y < m.y)
        {
            return 1 ; 
        }
        else {
            return 2 ; 
        }
    }

}

bounding_box_t Ball::get_boundingbox(){
    bounding_box_t b ;
    b.x = this->position.x ; 
    b.y = this->position.y ; 
    b.height = 0.5 ; 
    b.width = 0.2 ; 

    return b ; 
}

void Ball::tick() {
    // if no collision 
    if(this->position.y >= -1.75-Yvelocity)
    {   
        if(this->position.y + Yvelocity >= 2.8)
        {
            Yvelocity = 0 ;
        }
        else
        {
            this->position.y += Yvelocity ; 
            Yvelocity -= 0.02 ;
        }
    } 
    else
    { 
        Yvelocity = 0.0 ;
        this->position.y = -1.75 ; 
    } 
    // this->position.x -= speed;
    // this->position.y -= speed;
}


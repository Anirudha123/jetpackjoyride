#include "main.h"

#ifndef BALL_H
#define BALL_H


class Ball {
public:
    Ball() {}
    Ball(float x, float y, float s,  color_t color);
    glm::vec3 position;
    float rotation;
    void draw(glm::mat4 VP);
    void set_position(float x, float y);
    void set_speed(float s) ; 
    void set_yvelocity(float yv) ; 
    void tick();
    void left(float l) ; 
    void right(float r) ; 
    void jump(float v) ; 
    void up(float v) ; 
    void down(float v) ; 
    float return_x() ; 
    float return_y() ; 
    int magnet_pull_horizontal(bounding_box_t m) ; 
    int magnet_pull_vertical(bounding_box_t m) ; 
    bounding_box_t get_boundingbox(); 
private:
    double speed;
    // double gravity ;
    double Yvelocity ;  
    VAO *object_head;
    VAO *object_body;
    VAO *object_hair ; 
    VAO *object_legs ; 
    VAO *object_jetpack ; 
    VAO *object_eye ; 
};

#endif // BALL_H

#include "coin.h"
#include "main.h"

Coin::Coin(float x, float y, float r, color_t color,int type) {
    this->position = glm::vec3(x, y, 5);
    this->rotation = 0;
    this->speed = 0 ; 
    // Our vertices. Three consecutive floats give a 3D vertex; Three consecutive vertices give a triangle.
    // A cube has 6 faces with 2 triangles each, so this makes 6*2=12 triangles, and 12*3 vertices
    static GLfloat vertex_buffer_data[190] ; 
    int numberOfSides = 10 ; 
    this->radius = r ; 
    this->type = type  ; 
    for(int i = 0 ; i < numberOfSides ; i++)
		{
				vertex_buffer_data[9*i] = 0.2f ; 
				vertex_buffer_data[9*i+1] = -0.2f ; 
				vertex_buffer_data[9*i+2] = 0.0f ; 
				vertex_buffer_data[9*i+3] = radius * cos(2*M_PI*(i)/numberOfSides)+0.2 ; 
				vertex_buffer_data[9*i+4] = radius * sin(2*M_PI*(i)/numberOfSides) -0.2;
				vertex_buffer_data[9*i+5] = 0.0f ;
				vertex_buffer_data[9*i+6] = radius * cos(2*M_PI*(i+1)/numberOfSides) +0.2; 
				vertex_buffer_data[9*i+7] = radius * sin(2*M_PI*(i+1)/numberOfSides) - 0.2;
				vertex_buffer_data[9*i+8] = 0.0f ;
	    }

    this->object = create3DObject(GL_TRIANGLES, numberOfSides*3, vertex_buffer_data, color, GL_FILL);
}

void Coin::draw(glm::mat4 VP) {
    Matrices.model = glm::mat4(1.0f);
    glm::mat4 translate = glm::translate (this->position);    // glTranslatef
    this->rotation += 5 ; 
    glm::mat4 rotate    = glm::rotate((float) (this->rotation * M_PI / 180.0f), glm::vec3(0, 1, 0));
    // No need as coords centered at 0, 0, 0 of cube arouund which we waant to rotate
    // rotate          = rotate * glm::translate(glm::vec3(0, -0.6, 0));
    Matrices.model *= (translate * rotate);
    glm::mat4 MVP = VP * Matrices.model;
    glUniformMatrix4fv(Matrices.MatrixID, 1, GL_FALSE, &MVP[0][0]);
    draw3DObject(this->object);
}

void Coin::set_position(float x, float y) {
    this->position = glm::vec3(x, y, 0);
}

float Coin::return_x(){
    return this->position.x ; 
}

float Coin::return_y(){
    return this->position.y ; 
}

int Coin::return_type(){
    return this->type ; 
}

bounding_box_t Coin::get_boundingbox(){
    bounding_box_t b ;
    b.x = this->position.x ; 
    b.y = this->position.y ; 
    b.height =  2 * this->radius  ; 
    b.width = 2 * this->radius ; 

    return b ; 
}

void Coin::collected(){
    this->speed = 0.5 ; 
}


void Coin::tick() {
    //check collision, if collision, go to top right/left or whereever score is displayed 

    
    this->position.y += this->speed ;
    this->position.x -= this->speed ; 

}


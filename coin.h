#include "main.h"

#ifndef COIN_H
#define COIN_H


class Coin {
public:
    Coin() {}
    Coin(float x, float y, float r, color_t color, int type);
    glm::vec3 position;
    float rotation;
    void draw(glm::mat4 VP);
    void set_position(float x, float y); 
    void tick();
    float return_x() ; 
    float return_y() ; 
    void collected() ;
    int return_type() ; 
    bounding_box_t get_boundingbox() ; 
private:  
    VAO *object;
    float radius ; 
    float speed ; 
    int type ; 
    color_t color ; 
};

#endif //   Ground_H

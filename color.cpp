#include "main.h"

const color_t COLOR_RED = { 236, 100, 75 };
const color_t COLOR_GREEN = { 135, 211, 124 };
const color_t COLOR_BLACK = { 52, 73, 94 };
const color_t COLOR_BACKGROUND = { 242, 241, 239 };
const color_t COLOR_SKYBLUE = {135, 206, 235} ;
const color_t COLOR_YELLOW = {230,230,100} ; 
const color_t COLOR_ORANGE = {255, 165, 0} ; 
const color_t COLOR_VIOLET = {255,0,255} ; 
const color_t COLOR_CRIMSON = {220,20,60} ; 
const color_t COLOR_BLUE = {30,144,255} ; 
const color_t COLOR_SILVER = {192,192,192} ; 
const color_t COLOR_NAVY = {0,0,128} ; 
const color_t COLOR_CREAM = {255,205,148} ; 
const color_t COLOR_ICEBALL = {203, 254, 254} ; 
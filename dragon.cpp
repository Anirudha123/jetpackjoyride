#include "dragon.h"
#include "main.h"

Dragon::Dragon(float x, float y, color_t color) {
    this->position = glm::vec3(x, y, 0);
    this->rotation = 0;
    this->timer = 0 ;
    // Yvelovity = 0 ; 
    // gravity = 0.005 ;
    // Our vertices. Three consecutive floats give a 3D vertex; Three consecutive vertices give a triangle.
    // A cube has 6 faces with 2 triangles each, so this makes 6*2=12 triangles, and 12*3 vertices
    float length = 1.5 ; 
    float width = 7 ; 
    this->up = false ; 
    this-> ray_state = false ;

    static GLfloat vertex_buffer_data_one[] = {
        0.0,0.0,0.0, // head
        -0.2,0.0,0.0,
        0.0,0.2,0.0,
        0.0,0.0,0.0, //torso
        0.2,-0.6,0.0,
        0.4,-0.6,0.0,
        0.0,-0.6,0.0, // hip
        0.4,-0.6,0.0,
        0.4,-0.9,0.0,
        0.0,-0.8,0.0, // leg 1 
        0.2,-0.8,0.0,
        0.1,-0.7,0.0,
        0.2,-0.9,0.0, // leg 2 
        0.35,-0.9,0.0,
        0.3,-0.8,0.0,
        0.4,-0.6,0.0, //tail 
        0.4,-0.8,0.0,
        1.0, -1.3,0.0,
    } ;

    static GLfloat vertex_buffer_data_two[] = {
        0.2, -0.1, 0.0, // wing 1 
        0.9, -0.1, 0.0,
        0.5, 0.2, 0.0,
        0.2, -0.1, 0.0, // wing 2
        0.6, 0.0, 0.0,
        0.7, -0.2, 0.0,
        0.9,-1.2, 0.0, // tail tip
        1.0, -1.3, 0.0,
        1.2, -1.1, 0.0,
    } ; 

    static GLfloat vertex_buffer_data_three[] = {
        0.0,0.0,0.0, // belly
        0.0,-0.6,0.0,
        0.2,-0.6,0.0 , 
    } ; 
    

    this->object1 = create3DObject(GL_TRIANGLES, 6*3, vertex_buffer_data_one, COLOR_BLUE, GL_FILL);
    this->object2 = create3DObject(GL_TRIANGLES, 3*3, vertex_buffer_data_two, COLOR_NAVY, GL_FILL);
    this->object3 = create3DObject(GL_TRIANGLES, 1*3, vertex_buffer_data_three, COLOR_SKYBLUE, GL_FILL);

}

void Dragon::draw(glm::mat4 VP) {
    Matrices.model = glm::mat4(1.0f);
    glm::mat4 translate = glm::translate (this->position);    // glTranslatef
    glm::mat4 rotate    = glm::rotate((float) (this->rotation ), glm::vec3(0, 0, 1));
    // No need as coords centered at 0, 0, 0 of cube arouund which we waant to rotate
    // rotate          = rotate * glm::translate(glm::vec3(0, -0.6, 0));
    Matrices.model *= (translate * rotate);
    glm::mat4 MVP = VP * Matrices.model;
    glUniformMatrix4fv(Matrices.MatrixID, 1, GL_FALSE, &MVP[0][0]);
    // draw3DObject(this->object3) ;
    draw3DObject(this->object1);
    draw3DObject(this->object2) ; 
    draw3DObject(this->object3) ; 

    // if (this->timer > 180){
    //     draw3DObject(this->object3) ; 
    //     ray_state = true ; 
    // }
    // else { 
    //     ray_state = false ; 
    // }

}

bool Dragon::return_raystate(){
    return this->ray_state ; 
}

void Dragon::set_position(float x, float y) {
    this->position = glm::vec3(x, y, 0);
}

void Dragon::set_x(float x){
    this->position = glm::vec3(x, this->position.y, 0);
}

void Dragon::set_speed(float s){
    this->speed = s ; 
}

void Dragon::set_yvelocity(float yv){
    this->Yvelocity = yv ; 
}

float Dragon::return_x(){
    return this->position.x ; 
}

float Dragon::return_y(){
    return this->position.y ; 
}

void Dragon::speedx(float s){
    this->speed_x += s ; 
}

bounding_box_t Dragon::get_boundingbox(){
    bounding_box_t b ;
    b.x = this->position.x ; 
    b.y = this->position.y ; 
    b.height = 1.5 ; 
    b.width = this->rotation ; // width here is substituted for rotation

    return b ; 
}

void Dragon::tick(Ball b) {
    // if no collision
    float ball_y = b.return_y() ; 
    if(ball_y > this->position.y ) 
    {
        this->position.y += 0.04 ; 
    }
    else if(ball_y < this->position.y)
    {
        this->position.y -= 0.04 ; 
    }
    
    this->position.x += this->speed_x ; 
    // if(up == true) 
    //     this->position.y += 0.05 ;
    // else 
    //     this->position.y -= 0.05 ; 

    // if (this->position.y < -2.0)
    //     up =  true ; 

    // if (this->position.y > 2.5 )
    //     up = false ; 

    return ;    
}


#include "main.h"
#include "ball.h"

#ifndef DRAGON_H
#define DRAGON_H


class Dragon {
public:
    Dragon() {}
    Dragon(float x, float y, color_t color);
    glm::vec3 position;
    float rotation;
    void draw(glm::mat4 VP);
    void set_position(float x, float y);
    void set_speed(float s) ; 
    void set_yvelocity(float yv) ; 
    void set_x(float x) ;
    void tick(Ball b);
    float return_x() ; 
    void speedx(float s) ; 
    float return_y() ; 
    bool return_raystate() ; 
    bounding_box_t get_boundingbox(); 
private:
    double speed;
    float speed_x ; 
    // double gravity ;
    float radius ; 
    bool up ;
    bool ray_state ;  
    int timer ; 
    double Yvelocity ;  
    VAO *object1;
    VAO *object2;
    VAO *object3;
};

#endif // BALL_H

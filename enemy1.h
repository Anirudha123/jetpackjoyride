#include "main.h"

#ifndef ENEMY1_H
#define ENEMY1_H


class Fireline {
public:
    Fireline() {}
    Fireline(float x, float y, color_t color);
    glm::vec3 position;
    float rotation;
    void draw(glm::mat4 VP);
    void set_position(float x, float y);
    void set_speed(float s) ; 
    void set_yvelocity(float yv) ; 
    void tick();
    float return_x() ; 
    float return_y() ; 
    bounding_box_t get_boundingbox(); 
private:
    double speed;
    // double gravity ;
    float radius ; 
    double Yvelocity ;  
    VAO *object1;
    VAO *object2;
    VAO *object3;
};

#endif // BALL_H

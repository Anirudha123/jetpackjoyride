#include "enemy2.h"
#include "main.h"

Firebeam::Firebeam(float x, float y, color_t color) {
    this->position = glm::vec3(x, y, 0);
    this->rotation = 0;
    this->timer = 0 ;
    // Yvelovity = 0 ; 
    // gravity = 0.005 ;
    // Our vertices. Three consecutive floats give a 3D vertex; Three consecutive vertices give a triangle.
    // A cube has 6 faces with 2 triangles each, so this makes 6*2=12 triangles, and 12*3 vertices
    static GLfloat vertex_buffer_data_one[190], vertex_buffer_data_two[190] ; 
    int numberOfSides = 10 ; 
    this->radius = 0.5 ; 
    float length = 1.5 ; 
    float width = 7 ; 
    this->up = false ; 
    this->rotation = 0 ;
    this-> ray_state = false ; 
    for(int i = 0 ; i < numberOfSides ; i++)
		{
				vertex_buffer_data_one[9*i] = 0.15f ; 
				vertex_buffer_data_one[9*i+1] = -0.15f ; 
				vertex_buffer_data_one[9*i+2] = 0.0f ; 
				vertex_buffer_data_one[9*i+3] = radius * cos(2*M_PI*(i)/numberOfSides)+0.15 ; 
				vertex_buffer_data_one[9*i+4] = radius * sin(2*M_PI*(i)/numberOfSides) -0.15;
				vertex_buffer_data_one[9*i+5] = 0.0f ;
				vertex_buffer_data_one[9*i+6] = radius * cos(2*M_PI*(i+1)/numberOfSides) +0.15; 
				vertex_buffer_data_one[9*i+7] = radius * sin(2*M_PI*(i+1)/numberOfSides) - 0.15;
				vertex_buffer_data_one[9*i+8] = 0.0f ;
	    }

    static GLfloat vertex_buffer_data_three[] = {
        0.13f, 0.10f, 0.00f,
        0.13f + width, 0.10f, 0.0f,
        0.13f, -0.40f , 0.0f, 
        0.13f, -0.40f, 0.0f, 
        0.13f + width, -0.40f, 0.0f,
        0.13f + width, 0.10f, 0.0f , 
    } ; 
    

    for(int i = 0 ; i < numberOfSides ; i++)
		{
				vertex_buffer_data_two[9*i] = 0.15f + width; 
				vertex_buffer_data_two[9*i+1] = -0.15f; 
				vertex_buffer_data_two[9*i+2] = 0.0f ; 
				vertex_buffer_data_two[9*i+3] = radius * cos(2*M_PI*(i)/numberOfSides)+0.15 + width; 
				vertex_buffer_data_two[9*i+4] = radius * sin(2*M_PI*(i)/numberOfSides) -0.15;
				vertex_buffer_data_two[9*i+5] = 0.0f ;
				vertex_buffer_data_two[9*i+6] = radius * cos(2*M_PI*(i+1)/numberOfSides) +0.15 + width; 
				vertex_buffer_data_two[9*i+7] = radius * sin(2*M_PI*(i+1)/numberOfSides) - 0.15;
				vertex_buffer_data_two[9*i+8] = 0.0f ;
	    }


    this->object1 = create3DObject(GL_TRIANGLES, numberOfSides*3, vertex_buffer_data_one, COLOR_RED, GL_FILL);
    this->object2 = create3DObject(GL_TRIANGLES, numberOfSides*3, vertex_buffer_data_two, COLOR_RED, GL_FILL);
    this->object3 = create3DObject(GL_TRIANGLES, 2*3, vertex_buffer_data_three, color, GL_FILL);


}

void Firebeam::draw(glm::mat4 VP) {
    Matrices.model = glm::mat4(1.0f);
    glm::mat4 translate = glm::translate (this->position);    // glTranslatef
    glm::mat4 rotate    = glm::rotate((float) (this->rotation ), glm::vec3(0, 0, 1));
    // No need as coords centered at 0, 0, 0 of cube arouund which we waant to rotate
    // rotate          = rotate * glm::translate(glm::vec3(0, -0.6, 0));
    Matrices.model *= (translate * rotate);
    glm::mat4 MVP = VP * Matrices.model;
    glUniformMatrix4fv(Matrices.MatrixID, 1, GL_FALSE, &MVP[0][0]);
    // draw3DObject(this->object3) ;
    draw3DObject(this->object1);
    draw3DObject(this->object2) ; 

    if (this->timer > 180){
        draw3DObject(this->object3) ; 
        ray_state = true ; 
    }
    else { 
        ray_state = false ; 
    }

}

bool Firebeam::return_raystate(){
    return this->ray_state ; 
}

void Firebeam::set_position(float x, float y) {
    this->position = glm::vec3(x, y, 0);
}

void Firebeam::set_x(float x){
    this->position = glm::vec3(x, this->position.y, 0);
}

void Firebeam::set_speed(float s){
    this->speed = s ; 
}

void Firebeam::set_yvelocity(float yv){
    this->Yvelocity = yv ; 
}

float Firebeam::return_x(){
    return this->position.x ; 
}

float Firebeam::return_y(){
    return this->position.y ; 
}

bounding_box_t Firebeam::get_boundingbox(){
    bounding_box_t b ;
    b.x = this->position.x ; 
    b.y = this->position.y ; 
    b.height = 1.5 ; 
    b.width = this->rotation ; // width here is substituted for rotation

    return b ; 
}

void Firebeam::tick() {
    // if no collision
    if(up == true) 
        this->position.y += 0.01 ;
    else 
        this->position.y -= 0.01 ; 

    if (this->position.y < -2.5)
        up =  true ; 

    if (this->position.y > 2.5 )
        up = false ; 

    this->timer = (this->timer+1) % 300 ; 
    return ;    
}


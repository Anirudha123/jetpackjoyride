#include "enemy3.h"
#include "main.h"

Boomerang::Boomerang(float x, float y, float s, color_t color) {
    this->position = glm::vec3(x, y, 0);
    this->rotation = 0;
    this->Yvelocity = 0.1 ;
    speed = s;
    // Yvelovity = 0 ; 
    // gravity = 0.005 ;
    // Our vertices. Three consecutive floats give a 3D vertex; Three consecutive vertices give a triangle.
    // A cube has 6 faces with 2 triangles each, so this makes 6*2=12 triangles, and 12*3 vertices
    static const GLfloat vertex_buffer_data[] = {
        0.0f, 0.0f, 0.0f,
        0.3f, 0.0, 0.0,
        0.0 , -0.2, 0.0,
        0.0, 0.0, 0.0,
        0.0, -0.3, 0.0,
        0.2, 0.0, 0.0,
    };

    static GLfloat vertex_buffer_data_head[190] ; 
    int numberOfSides = 10 ; 
    float radius = 0.1 ; 
    for(int i = 0 ; i < numberOfSides ; i++)
		{
				vertex_buffer_data_head[9*i] = 0.1f ; 
				vertex_buffer_data_head[9*i+1] = -0.1f ; 
				vertex_buffer_data_head[9*i+2] = 0.0f ; 
				vertex_buffer_data_head[9*i+3] = radius * cos(2*M_PI*(i)/numberOfSides) + 0.1 ; 
				vertex_buffer_data_head[9*i+4] = radius * sin(2*M_PI*(i)/numberOfSides) - 0.1;
				vertex_buffer_data_head[9*i+5] = 0.0f ;
				vertex_buffer_data_head[9*i+6] = radius * cos(2*M_PI*(i+1)/numberOfSides) + 0.1; 
				vertex_buffer_data_head[9*i+7] = radius * sin(2*M_PI*(i+1)/numberOfSides) - 0.1;
				vertex_buffer_data_head[9*i+8] = 0.0f ;
	    }

    this->object_head = create3DObject(GL_TRIANGLES, numberOfSides*3, vertex_buffer_data_head, COLOR_BLACK, GL_FILL);

    this->object_body = create3DObject(GL_TRIANGLES, 4*3, vertex_buffer_data, color, GL_FILL);
}

void Boomerang::draw(glm::mat4 VP) {
    Matrices.model = glm::mat4(1.0f);
    glm::mat4 translate = glm::translate (this->position);    // glTranslatef
    glm::mat4 rotate    = glm::rotate((float) (this->rotation * M_PI / 180.0f), glm::vec3(0, 0, 1));
    this->rotation += 10;
    // No need as coords centered at 0, 0, 0 of cube arouund which we waant to rotate
    // rotate          = rotate * glm::translate(glm::vec3(0, -0.6, 0));
    Matrices.model *= (translate * rotate);
    glm::mat4 MVP = VP * Matrices.model;
    glUniformMatrix4fv(Matrices.MatrixID, 1, GL_FALSE, &MVP[0][0]);
    draw3DObject(this->object_head) ;
    draw3DObject(this->object_body);
}

void Boomerang::set_position(float x, float y) {
    this->position = glm::vec3(x, y, 0);
}

void Boomerang::left(float l){
    this->position.x -= l  ; 
}

void Boomerang::right(float r){
    this->position.x += r ; 
}

void Boomerang::up(float v){
    this->set_yvelocity(v) ; 
}

void Boomerang::set_speed(float s){
    this->speed = s ; 
}

void Boomerang::set_yvelocity(float yv){
    this->Yvelocity = yv ; 
}

float Boomerang::return_x(){
    return this->position.x ; 
}

float Boomerang::return_y(){
    return this->position.y ; 
}

bounding_box_t Boomerang::get_boundingbox(){
    bounding_box_t b ;
    b.height = 0.1 ; 
    b.width = 0.1 ; 
    b.x = this->position.x - 0.1;
    b.y = this->position.y + 0.1; 

    return b ; 
}

void Boomerang::tick() {
    // if no collision 

    this->position.x -= speed ;
    this->speed -= 0.01 ; 
    this->position.y -= Yvelocity ;   




    // if(this->position.y >= -1.75-Yvelocity)
    // {   
    //     if(this->position.y + Yvelocity >= 2.8)
    //     {
    //         Yvelocity = 0 ;
    //     }
    //     else
    //     {
    //         this->position.y += Yvelocity ; 
    //         Yvelocity -= 0.02 ;
    //     }
    // } 
    // else
    // { 
    //     Yvelocity = 0.0 ;
    //     this->position.y = -1.75 ; 
    // } 
    // this->position.x -= speed;
    // this->position.y -= speed;
}


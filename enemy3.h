#include "main.h"

#ifndef ENEMY3_H
#define ENEMY3_H


class Boomerang {
public:
    Boomerang() {}
    Boomerang(float x, float y, float s,  color_t color);
    glm::vec3 position;
    float rotation;
    void draw(glm::mat4 VP);
    void set_position(float x, float y);
    void set_speed(float s) ; 
    void set_yvelocity(float yv) ; 
    void tick();
    void left(float l) ; 
    void right(float r) ; 
    void up(float v) ; 
    float return_x() ; 
    float return_y() ; 
    bounding_box_t get_boundingbox(); 
private:
    double speed;
    // double gravity ;
    double Yvelocity ;  
    VAO *object_head;
    VAO *object_body;
};

#endif // BALL_H

#include "main.h"

#ifndef ICEBALL_H
#define ICEBALL_H


class Iceball {
public:
    Iceball() {}
    Iceball(float x, float y, float r, color_t color);
    glm::vec3 position;
    float rotation;
    void draw(glm::mat4 VP);
    void set_position(float x, float y); 
    void tick();
    float return_x() ; 
    float return_y() ; 
    void collected() ; 
    bounding_box_t get_boundingbox() ; 
private:  
    VAO *object;
    VAO *fins ; 
    float radius ; 
    float speed ; 
    int type ; 
    color_t color ; 
};

#endif //   Ground_H

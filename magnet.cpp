#include "magnet.h"
#include "main.h"

Magnet::Magnet(float x, float y, color_t color) {
    this->position = glm::vec3(x, y, 0);
    this->rotation = 0;
    this->Yvelocity = 0.1 ;
    // Yvelovity = 0 ; 
    // gravity = 0.005 ;
    // Our vertices. Three consecutive floats give a 3D vertex; Three consecutive vertices give a triangle.
    // A cube has 6 faces with 2 triangles each, so this makes 6*2=12 triangles, and 12*3 vertices
    static const GLfloat vertex_buffer_data_head[] = {
        0.0f, 0.0f, 0.0f,
        0.0f, -0.2, 0.0,
        0.7 , -0.2, 0.0,
        0.0, 0.0, 0.0,
        0.7, 0.0, 0.0,
        0.7, -0.2, 0.0,
    };

    static GLfloat vertex_buffer_data_prongs[] = {
        0.0, 0.0, 0.0,
        0.0, -0.5, 0.0,
        0.2, 0.0, 0.0, 
        0.2, -0.5, 0.0,
        0.0, -0.5, 0.0,
        0.2, 0.0, 0.0, 

        0.5, 0.0, 0.0,
        0.5, -0.5, 0.0,
        0.7, -0.5, 0.0,
        0.7, 0.0, 0.0, 
        0.5, 0.0, 0.0,
        0.7, -0.5, 0.0
    } ; 

    this->object_head = create3DObject(GL_TRIANGLES, 2*3, vertex_buffer_data_head, COLOR_RED, GL_FILL);

    this->object_body = create3DObject(GL_TRIANGLES, 4*3, vertex_buffer_data_prongs, color, GL_FILL);
}

void Magnet::draw(glm::mat4 VP) {
    Matrices.model = glm::mat4(1.0f);
    glm::mat4 translate = glm::translate (this->position);    // glTranslatef
    glm::mat4 rotate    = glm::rotate((float) (this->rotation * M_PI / 180.0f), glm::vec3(1, 0, 0));
    // No need as coords centered at 0, 0, 0 of cube arouund which we waant to rotate
    // rotate          = rotate * glm::translate(glm::vec3(0, -0.6, 0));
    Matrices.model *= (translate * rotate);
    glm::mat4 MVP = VP * Matrices.model;
    glUniformMatrix4fv(Matrices.MatrixID, 1, GL_FALSE, &MVP[0][0]);
    draw3DObject(this->object_body);
    draw3DObject(this->object_head) ;
}

void Magnet::set_position(float x, float y) {
    this->position = glm::vec3(x, y, 0);
}

float Magnet::return_x(){
    return this->position.x ; 
}

float Magnet::return_y(){
    return this->position.y ; 
}

bounding_box_t Magnet::get_boundingbox(){
    bounding_box_t b ;
    b.height = 0.1 ; 
    b.width = 0.1 ; 
    b.x = this->position.x - 0.1;
    b.y = this->position.y + 0.1; 

    return b ; 
}

void Magnet::tick(Ball b, int i[]) {

    i[0] = b.magnet_pull_horizontal(this->get_boundingbox()) ; 
    i[1] = b.magnet_pull_vertical(this->get_boundingbox()) ; 

}


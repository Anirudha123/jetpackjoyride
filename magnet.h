#include "main.h"
#include "ball.h"

#ifndef MAGNET_H
#define MAGNET_H


class Magnet {
public:
    Magnet() {}
    Magnet(float x, float y, color_t color);
    glm::vec3 position;
    float rotation;
    void draw(glm::mat4 VP);
    void set_position(float x, float y);
    // void set_speed(float s) ; 
    // void set_yvelocity(float yv) ; 
    void tick(Ball b, int i[]); 
    void left(float l) ; 
    // void right(float r) ; 
    // void up(float v) ; 
    float return_x() ; 
    float return_y() ; 
    bounding_box_t get_boundingbox(); 
private:
    double speed;
    // double gravity ;
    double Yvelocity ;  
    VAO *object_head;
    VAO *object_body;
};

#endif // BALL_H

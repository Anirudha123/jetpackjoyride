#include "main.h"
#include "timer.h"
#include "ball.h"
#include "ground.h"
#include "wall.h"
#include "coin.h"
#include "enemy1.h"
#include "thruster.h"
#include "enemy2.h"
#include "powerup.h"
#include "waterballoon.h"
#include "enemy3.h"
#include "magnet.h"
#include "scoreboard.h"
#include "semicircularring.h"
#include "dragon.h"
#include "iceball.h"

using namespace std;

GLMatrices Matrices;
GLuint     programID;
GLFWwindow *window;

/**************************
* Customizable functions *
**************************/

Ball ball1 ; 
Dragon dragon ; 
Ground darkBlock[2000], lightBlock[2000] ; 
Wall wallList[2000] ; 
vector <Coin> coin ; 
vector <Fireline> fline ; 
vector <Thruster> exhaust ; 
vector <Firebeam> fbeam ; 
vector <Powerup> powerup ; 
vector <Waterballoon> wb ;
vector <Boomerang> boomerang ; 
vector <Magnet> magnet ; 
vector <Scoreboard> sboard ; 
vector <Highway> highway ; 
vector <Iceball> iceball ; 


float screen_zoom = 1, screen_center_x = 0, screen_center_y = 0;
float camera_rotation_angle = 0;
float frameOfReference = 0 ;
float timer_bc = 0 ;
bool bonus_coins = false ;
float timer_sp = 0 ; 
float jetpack_speed = 0.07 ; 
long long score = 0 ; 
int magnet_timer = 0 ;
int magnet_count = 0 ;
int level = 1 ; 
bool in_highway = false ; 
bool dragon_exists = false ; 

Timer t60(1.0 / 30);

/* Render the scene with openGL */
/* Edit this function according to your assignment */
void draw() {
    // clear the color and depth in the frame buffer
    glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // use the loaded shader program
    // Don't change unless you know what you are doing
    glUseProgram (programID);

    // Eye - Location of camera. Don't change unless you are sure!!
    glm::vec3 eye ( frameOfReference, 0, 20.0);
    // Target - Where is the camera looking at.  Don't change unless you are sure!!
    glm::vec3 target (frameOfReference, 0, 0);
    // Up - Up vector defines tilt of camera.  Don't change unless you are sure!!
    glm::vec3 up (0, level, 0);

    // Compute Camera matrix (view)
    Matrices.view = glm::lookAt( eye, target, up ); // Rotating Camera for 3D
    // Don't change unless you are sure!!
    // Matrices.view = glm::lookAt(glm::vec3(0, 0, 3), glm::vec3(0, 0, 0), glm::vec3(0, 1, 0)); // Fixed camera for 2D (ortho) in XY plane

    // Compute ViewProject matrix as view/camera might not be changed for this frame (basic scenario)
    // Don't change unless you are sure!!
    glm::mat4 VP = Matrices.projection * Matrices.view;

    // Send our transformation to the currently bound shader, in the "MVP" uniform
    // For each model you render, since the MVP will be different (at least the M part)
    // Don't change unless you are sure!!
    glm::mat4 MVP;  // MVP = Projection * View * Model

    // Scene render

    for(int i=0 ; i < 2000 ; ++i){
        if(abs(int(darkBlock[i].return_x())-int(frameOfReference))<10)
        {
            darkBlock[i].draw(VP) ;
            lightBlock[i].draw(VP) ;
        }
    }

    for(int i=0; i<1000 ; ++i){
        if(abs(int(wallList[i].return_x())-int(frameOfReference))<10)
        {
            wallList[i].draw(VP) ;
        }
    }

    for(int i = 0 ; i < highway.size() ; ++i)
            highway[i].draw(VP) ; 

    for(int i=0; i<coin.size() ; ++i){
        if(abs(int(coin[i].return_x())-int(frameOfReference))<10)
        {
            if(coin[i].return_type() == 0 || coin[i].return_type() == 1)
                coin[i].draw(VP) ; 

            if(coin[i].return_type() == 2 && timer_bc > 0 )
                coin[i].draw(VP) ;
        }
    }

    for(int i=0; i< fline.size() ; ++i)
    {  
        if(abs((int)fline[i].return_x()) - ((int)(frameOfReference)) < 10)  
        fline[i].draw(VP) ;
    } 

    for(int i = 0 ; i < exhaust.size() ; ++i)
    {
        exhaust[i].draw(VP) ; 
    }

    for(int i = 0 ; i < powerup.size() ; ++i)
    {
        if(abs((int)powerup[i].return_x()) - ((int)(frameOfReference)) < 10)  
            powerup[i].draw(VP) ; 
    }

    for(int i = 0 ; i < wb.size() ; ++i)
    {
        wb[i].draw(VP) ; 
    }

    for(int i = 0 ; i < boomerang.size() ; ++i)
    {   
            boomerang[i].draw(VP) ; 
    }

    for(int i = 0 ; i < fbeam.size() ; ++i){
        fbeam[i].set_x(frameOfReference - 3.65 ) ;
        fbeam[i].draw(VP) ;
    }

    for(int i = 0 ; i < magnet.size() ; ++i)
        magnet[i].draw(VP) ; 

    for(int i = 0 ; i < iceball.size() ; ++i)
        iceball[i].draw(VP) ; 

    int temp = score + frameOfReference; 
    float fr = frameOfReference ; 
    float i = 0 ;

    while(temp!=0)
    {
        sboard[i].set_position(fr-2.5 - i*0.3, 3.5) ; 
        sboard[i].draw(VP, temp%10) ; 
        i++ ; 
        temp/=10 ;
    }

    if(dragon_exists == true)
    {
        dragon.set_x(frameOfReference + 3) ;
        dragon.draw(VP) ; 
    }

    ball1.draw(VP);
}

void tick_input(GLFWwindow *window) {
    int left  = glfwGetKey(window, GLFW_KEY_LEFT);
    int right = glfwGetKey(window, GLFW_KEY_RIGHT); 
    int up = glfwGetKey(window, GLFW_KEY_UP) ; 
    int w = glfwGetKey(window, GLFW_KEY_W) ; 

    glfwSetScrollCallback(window, scroll_callback) ; 

    if (left) {
        if(ball1.return_x() >= frameOfReference - 4)
        ball1.left(jetpack_speed) ; 
    }
    if (right) {
        ball1.right(jetpack_speed) ;  
        if (ball1.return_x() >= frameOfReference)
            frameOfReference += jetpack_speed ;  
    }
    if(up){
        ball1.jump(jetpack_speed) ;  
        // exhaust.push_back(Thruster(ball1.return_x() + 0.3, ball1.return_y() - 1.0 , 0,  COLOR_YELLOW )) ;
        exhaust.push_back(Thruster(ball1.return_x() , ball1.return_y() - 0.75 , 0,  COLOR_YELLOW )) ; 
    }

    if(w){
        wb.push_back(Waterballoon(ball1.return_x(), ball1.return_y(), 0.25, COLOR_BLUE )) ; 
    }

    ball1.right(0.03) ;
    frameOfReference += 0.03 ; 
}

void exhaust_reaper(){
    for(int i = 0 ; i < exhaust.size() ; ++i)
    {
        if(exhaust[i].return_y() < -3)
        {
            exhaust.erase(exhaust.begin()+i) ; 
        }
    }

    return ; 
}

void coin_collision(){
    
    bounding_box_t c, b ;
    b = ball1.get_boundingbox() ; 

    for (int i = 0 ; i < coin.size() ; ++i) 
    {
        if(abs((int)coin[i].return_x()-(int)ball1.return_x()) < 10)
        {
            if(coin[i].return_type() == 0){
                c = coin[i].get_boundingbox() ;
                if(detect_collision(b,c)){
                    coin[i].collected() ; 
                    score+=10 ; 
                }
                coin[i].tick() ;
            }
            else if(coin[i].return_type() == 1) {
                c = coin[i].get_boundingbox() ;
                if(detect_collision(b,c)){
                    coin[i].collected() ;
                    score+=20 ;  
                }
                coin[i].tick() ;
            }
            else if(coin[i].return_type() == 2 && timer_bc > 0 ) {
                c = coin[i].get_boundingbox() ;
                if(detect_collision(b,c)){
                    coin[i].collected() ;
                    score+=40 ;  
                }
                coin[i].tick() ;
            }
        }

        if(((int)ball1.return_x() - (int)coin[i].return_x())>10){
            coin.erase(coin.begin()+i) ; 
        }
    }

    return ; 
}

void iceball_collision(){
    
    bounding_box_t c, b ;
    b = ball1.get_boundingbox() ; 

    for(int i = 0 ; i < iceball.size() ; ++i)
    {
        c = iceball[i].get_boundingbox() ;
        if(detect_collision(b,c)){
            exit(0) ;
        }

        if(((int)ball1.return_x() - (int)iceball[i].return_x())>10){
                iceball.erase(iceball.begin()+i) ; 
        }
    }


}

void powerup_collision(){
     bounding_box_t p, b ;
    b = ball1.get_boundingbox() ; 

    for (int i = 0 ; i < powerup.size() ; ++i) 
    {
        p = powerup[i].get_boundingbox() ;
        
        if(abs((int)powerup[i].return_x()-(int)ball1.return_x()) < 10)
        {
            if(detect_collision(b,p)){
                if(powerup[i].return_type() == 0)
                {
                    bonus_coins = true ;
                    timer_bc = 360 ;  
                    powerup.erase(powerup.begin() + i) ; 
                }
                else if(powerup[i].return_type() == 1)
                {
                    timer_sp = 240 ; 
                    powerup.erase(powerup.begin()+ i) ; 
                }
            }
            powerup[i].tick() ; 
        }

        if(((int)ball1.return_x() - (int)powerup[i].return_x())>10){
            powerup.erase(powerup.begin()+i) ; 
        }
    }

    return ; 
}

bool detect_collision_fline(bounding_box_t a, bounding_box_t b)
{
    float dist_segment ;
    float fline_x, fline_y, fline_height, fline_rotation, fline_x2, fline_y2 ; 
    float ball_x, ball_y, ball_height, ball_breadth ;

    fline_x = a.x ; fline_y = a.y ; fline_height = a.height ; fline_rotation = a.width ;

    fline_x2 = fline_x + fline_height * sin(fline_rotation) ;
    fline_y2 = fline_y - fline_height * cos(fline_rotation) ;


    ball_x = b.x ; ball_y = b.y ; ball_height = b.height ; ball_breadth = b.width ; 

    // cout<<fline_x << " : " << fline_y << " : " << fline_x2 << " : "<< fline_y2 << " :: " << ball_x << " : " << ball_y << "\n"  ;

    float r_num = (ball_x - fline_x )*(fline_x2 - fline_x) + (ball_y - fline_y) * (fline_y2 - fline_y) ;
    float r_den = (fline_x2 - fline_x) * (fline_x2 - fline_x) + (fline_y2 - fline_y) * (fline_y2 - fline_y) ;
    float r = r_num / r_den ; 

    float px = fline_x + r*(fline_x2-fline_x) ;
    float py = fline_y + r*(fline_y2-fline_y) ; 

    float s = ((fline_y - ball_y) * (fline_x2 - fline_x) - (fline_x - ball_x) * (fline_y2 - fline_y)) / r_den ;

    float dist_line = fabs(s)*sqrt(r_den) ;

    float xx = px, yy =py ;

    if ( (r >= 0) && (r <= 1) )
	{
        // cout<<"YESS\n" ; 
		dist_segment = dist_line;
	}
	else
	{
        // cout<<"NOOOO\n" ;
		double dist1 = (ball_x-fline_x)*(ball_x-fline_x) + (ball_y-fline_y)*(ball_y-fline_y);
		double dist2 = (ball_x-fline_x2)*(ball_x-fline_x2) + (ball_y-fline_y2)*(ball_y-fline_y2);
		if (dist1 < dist2)
		{
			xx = fline_x;
			yy = fline_y;
			dist_segment = sqrt(dist1);
		}
		else
		{
			xx = fline_x2;
			yy = fline_y2;
			dist_segment = sqrt(dist2);
		}
	}   


    if(dist_segment  <= 0.1 )
    {
        // cout<<dist_segment<<"BOOOOBs "<<"x : "<<xx<<"y : "<<yy<<'\n' ;
        return true ;
    }

    else{
        // cout<<dist_segment<<"TITS "<<"x : "<<xx<<"y : "<<yy<<'\n' ;
        return false ;
    }

}

void fline_collision(){
    bounding_box_t b1,b2,b3,b4,b5,b6,b7,b8,b9, f ;

    b1 = ball1.get_boundingbox() ;

    b2 = b3 = b4 = b5 = b6 = b7 = b8 = b9 = b1 ; 
    b2.x = b2.x + b2.width ; 
    b3.x = b3.x + b3.width ; b3.y = b3.y - b3.height ;
    b4.y = b4.y - b4.height ;
    b6.x = b6.x + b2.width/2 ; 
    b7.x = b7.x + b7.width ; b7.y = b7.y - b7.height/2 ;
    b8.y = b8.y - b8.height/2;
    b9.x = b9.x + b9.width/2 ; b9.y = b9.y - b9.height ; 

    for(int i = 0 ; i < fline.size(); ++i)
    {
        if(abs((int)fline[i].return_x() - (int)ball1.return_x()) < 10)
        {   
            f = fline[i].get_boundingbox() ;

            if(detect_collision_fline(f,b1)){
                // cout<<"1\n" ; 
                exit(0) ;  
            }
            else if(detect_collision_fline(f,b2) || detect_collision_fline(f,b5))
            {
                // cout<<"2\n" ;
                exit(0) ;
            }
            else if(detect_collision_fline(f,b3)  || detect_collision_fline(f,b6))
            {
                // cout<<"3\n" ;
                exit(0) ;
            }
            else if(detect_collision_fline(f,b4) || detect_collision_fline(f,b7))
            {
                // cout<<"4\n" ; 
                exit(0) ;
            }
            else if(detect_collision_fline(f,b8) || detect_collision_fline(f,b9))
            {
                // cout<<"5\n" ; 
                exit(0) ; 
            }

            fline[i].tick() ; 
        }

        if(((int)ball1.return_x() - (int)fline[i].return_x())>10){
            fline.erase(fline.begin()+i) ; 
        }
    }
}

void fbeam_collision(){
    bounding_box_t b ; 
    b = ball1.get_boundingbox() ; 
    for(int i = 0 ; i < fbeam.size() ; ++i)
    {
        if(fbeam[i].return_raystate() == true)
        {
            float beam_y = fbeam[i].return_y() ;
            if(b.y - 2 * b.height < beam_y && b.y > beam_y - b.height)
            {
                exit(0) ; 
            }
        } 
    }

}

void waterballoon_collision(){


    bounding_box_t b1,b2,b3,b4,b5,b6,b7,b8,b9, f ;

    for(int j = 0 ; j < wb.size() ; ++j)
    {
        b1 = wb[j].get_boundingbox() ;

        b2 = b3 = b4 = b5 = b6 = b7 = b8 = b9 = b1 ; 
        b2.x = b2.x + b2.width ; 
        b3.x = b3.x + b3.width ; b3.y = b3.y - b3.height ;
        b4.y = b4.y - b4.height ;
        b6.x = b6.x + b2.width/2 ; 
        b7.x = b7.x + b7.width ; b7.y = b7.y - b7.height/2 ;
        b8.y = b8.y - b8.height/2;
        b9.x = b9.x + b9.width/2 ; b9.y = b9.y - b9.height ; 

        for(int i = 0 ; i < fline.size(); ++i)
        {
            if(abs((int)fline[i].return_x() - (int)wb[0].return_x()) < 10)
            {   
                f = fline[i].get_boundingbox() ;

                if(detect_collision_fline(f,b1)){
                    // cout<<"1\n" ; 
                    wb.erase(wb.begin() +j) ; 
                    fline.erase(fline.begin() + i) ;  
                    score+=30 ; 
                    continue ; 
                }
                else if(detect_collision_fline(f,b2) || detect_collision_fline(f,b5))
                {
                    // cout<<"2\n" ;
                    score+=30 ; 
                    wb.erase(wb.begin() +j) ;
                    fline.erase(fline.begin() + i) ;
                    continue ; 
                }
                else if(detect_collision_fline(f,b3)  || detect_collision_fline(f,b6))
                {
                    // cout<<"3\n" ;
                    score+=30 ; 
                    wb.erase(wb.begin() +j) ;
                    fline.erase(fline.begin() + i) ;
                    continue ; 
                }
                else if(detect_collision_fline(f,b4) || detect_collision_fline(f,b7))
                {
                    // cout<<"4\n" ;
                    score+=30 ;  
                    wb.erase(wb.begin() +j) ;
                    fline.erase(fline.begin() + i) ;
                    continue ; 
                }
                else if(detect_collision_fline(f,b8) || detect_collision_fline(f,b9))
                {
                    // cout<<"5\n" ;
                    score+=30 ;  
                    wb.erase(wb.begin() +j) ;
                    fline.erase(fline.begin() + i) ; 
                    continue ; 
                }
            }
        }
    }
}

void boomerang_collision(){
    bounding_box_t c, b ;
    b = ball1.get_boundingbox() ; 

    for (int i = 0 ; i < boomerang.size() ; ++i) 
    {
        if(abs((int)boomerang[i].return_x()-(int)ball1.return_x()) < 10)
        {
            c = boomerang[i].get_boundingbox() ;
            if(detect_collision(b,c)){
                exit(0) ; 
                score+=10 ; 
            }
        }

        if(abs((int)ball1.return_x() - (int)boomerang[i].return_x())>10){
            boomerang.erase(boomerang.begin()+i) ; 
        }
    }

    return ;    
}

void highway_collision(){
    //if you get in the radius, then you collide, if you collide, follow semicircular path 
    float centre_x, centre_y, radius, ball_x, ball_y ; 

    centre_x = highway[0].return_x() ; 
    centre_y = highway[0].return_y() ;
    radius = highway[0].return_radius() ; 

    ball_x = ball1.return_x() ; 
    ball_y = ball1.return_y() ; 


    if(((centre_x-ball_x)*(centre_x-ball_x) + (centre_y - ball_y)*(centre_y - ball_y) <= (radius * radius) + 1 ) && ((centre_x-ball_x)*(centre_x-ball_x) + (centre_y - ball_y)*(centre_y - ball_y) >= (radius * radius) - 1) && ball_y >= centre_y )
    {
        in_highway = true ;     
    }
}

void tick_elements() {
    
    if(in_highway == false){
        for(int i = 0 ; i < magnet.size() ; ++i)
        {
            int p[2] ;
            magnet[i].tick(ball1, p) ; 

            if(p[0] == 1)
            {
                ball1.right(0.02) ;  
            }
            else if( p[0] == 2 && ball1.return_x() + 4 > frameOfReference)
            {
                ball1.left(0.02) ; 
            }

            if(p[1] == 1)
            {
                ball1.up(0.3) ;
            }
            else if(p[1] == 2)
            {
                ball1.down(0.02) ; 
            }
        }
    }

    ball1.tick();
    if(dragon_exists == true)
        dragon.tick(ball1) ; 
    
    for(int i = 0 ; i < fbeam.size() ; ++i)
        fbeam[i].tick(); 


    for(int i = 0 ; i < iceball.size() ; ++i)
        iceball[i].tick() ; 

    for(int i = 0; i < exhaust.size() ; ++i )
    {
        exhaust[i].tick() ; 
    }   
    
    for(int i=0 ; i < wb.size() ; ++i)
    {
        wb[i].tick() ; 
        if(wb[i].return_x() >= frameOfReference + 10)
            wb.erase(wb.begin() + i) ; 
    }

    for(int i = 0 ; i < boomerang.size() ; ++i)
        boomerang[i].tick() ; 


    timer_bc--;

}

void highway_path(){

    float centre_x, centre_y, radius, ball_x, ball_y ; 
    centre_x = highway[0].return_x() ; 
    centre_y = highway[0].return_y() ;
    radius = highway[0].return_radius() ; 
    ball_x = ball1.return_x() ; 
    ball_y = ball1.return_y() ; 

    if(((radius*radius) - ((ball_x + jetpack_speed)-centre_x)*((ball_x + jetpack_speed)-centre_x)) > 0.0)
       {
            float new_y = sqrt((radius*radius) - ((ball_x + jetpack_speed)-centre_x)*((ball_x + jetpack_speed)-centre_x)) + centre_y ; 
            ball1.set_position(ball_x + jetpack_speed, new_y) ; 
            in_highway = true ; 

            // if(new_y < centre_y)
            //     in_highway = false ; 
       }
    else
        {
            in_highway = false ; 
            ball1.right(2*jetpack_speed) ; 
            highway.erase(highway.begin()) ; 
        }

    if (ball1.return_x() >= frameOfReference)
            frameOfReference += jetpack_speed ;

    return ;
}
/* Initialize the OpenGL rendering properties */
/* Add all the models to be created here */
void initGL(GLFWwindow *window, int width, int height) {
    /* Objects should be created before any other gl function and shaders */
    // Create the models

    ball1       = Ball(-2, -0.75, 0, COLOR_RED);

    for(int i = 0 ; i <10 ; i++)
        highway.push_back(Highway(rand()%30 + 8 + i*(32),-2.75,4,COLOR_GREEN)) ; 

    for(int i = 0 ; i < 5 ; i++)
    {
        powerup.push_back(Powerup((rand()%20 + 2) * 3*i, 2, 0.02, COLOR_VIOLET, 0)) ; 
        powerup.push_back(Powerup((rand()%30 + 4) * 4*i, 2, 0.01, COLOR_CRIMSON, 1 )) ; 
    }

    for(int i=0; i <1000 ; ++i)
    {
        int ran = rand() % 2 + -1 ;
        coin.push_back(Coin(-2+ran+i*2.5, -0.5+ran,0.2,  COLOR_YELLOW, 0)) ;
    } 

    for(int i=0 ; i < 1000 ; ++i){
        int ran = rand() % 10 * 0.2 - 1 ; 
        fline.push_back(Fireline(3+ran+i*3.8, 1+ran, COLOR_YELLOW)) ; 
    }
    
    for(int i=0 ; i < 1000 ; ++i){
        darkBlock[i]      = Ground(-2.75+i*2.5, -2.75, COLOR_BLACK) ;  
        lightBlock[i]     = Ground(-1.5+i*2.5,-2.75, COLOR_GREEN) ; 
        wallList[i]       = Wall(-1 + i*3, 1, COLOR_SKYBLUE) ;
    }

    for(int i=0 ; i < 1000 ; ++i){
        darkBlock[i+1000]      = Ground(-2.75+i*2.5,4.0, COLOR_BLACK) ;  
        lightBlock[i+1000]     = Ground(-1.5+i*2.5,4.0, COLOR_GREEN) ; 
    }

    for(int i=0 ; i <1000 ; ++i){
        int ran = rand() % 4 + -2 ;
        coin.push_back(Coin(-2.2+ran+i*2.5, 0.0+ran,0.2,  COLOR_GREEN, 1)) ;   
    }

    for(int i=0 ; i <1000 ; ++i){
        int ran = rand() % 4 + -2 ;
        coin.push_back(Coin(-2.2+ran+i*2.5, 0.0+ran,0.3,  COLOR_ORANGE, 2)) ;   
    }

    for(int i=0 ; i < 7 ; i++)
    {
        sboard.push_back(Scoreboard(2, 3, COLOR_NAVY)) ; 
    }

    // Create and compile our GLSL program from the shaders
    programID = LoadShaders("Sample_GL.vert", "Sample_GL.frag");
    // Get a handle for our "MVP" uniform
    Matrices.MatrixID = glGetUniformLocation(programID, "MVP");


    reshapeWindow (window, width, height);

    // Background color of the scene
    glClearColor (COLOR_BACKGROUND.r / 256.0, COLOR_BACKGROUND.g / 256.0, COLOR_BACKGROUND.b / 256.0, 0.0f); // R, G, B, A
    glClearDepth (1.0f);

    glEnable (GL_DEPTH_TEST);
    glDepthFunc (GL_LEQUAL);

    cout << "VENDOR: " << glGetString(GL_VENDOR) << endl;
    cout << "RENDERER: " << glGetString(GL_RENDERER) << endl;
    cout << "VERSION: " << glGetString(GL_VERSION) << endl;
    cout << "GLSL: " << glGetString(GL_SHADING_LANGUAGE_VERSION) << endl;
}


int main(int argc, char **argv) {
    srand(time(0));
    int width  = 900;
    int height = 900;

    window = initGLFW(width, height);

    initGL (window, width, height);

    /* Draw in loop */
    while (!glfwWindowShouldClose(window)) {
        // Process timers

        if (t60.processTick()) {
            // 60 fps
            // OpenGL Draw commands
            if(timer_sp > 0)
            {
                timer_sp-- ; 
                jetpack_speed = 0.15 ; 
            }
            else {
                jetpack_speed = 0.07 ; 
            }

            if(ball1.return_x() > 100)
                level = -1 ; 

            draw();
            // Swap Frame Buffer in double buffering
            glfwSwapBuffers(window);

                        tick_elements();
            highway_collision() ; 

            if(ball1.return_x() > 8 && ball1.return_x() < 30 && fbeam.size() == 0)
            {
                fbeam.push_back(Firebeam(2, 2, COLOR_ORANGE));
            }

            if(ball1.return_x() > 38 && fbeam.size()!=0)
            {
                fbeam.erase(fbeam.begin()) ; 
            }

            if(ball1.return_x() > 50 && dragon_exists == false && ball1.return_x() < 80 ) 
            {
                dragon      = Dragon(frameOfReference + 2 ,2,COLOR_BLACK) ; 
                dragon_exists = true ;
            }
            if(ball1.return_x() > 80)
            {
                dragon_exists = false ; 
            }

            if(rand() % 100 == 0)
            {
                   boomerang.push_back(Boomerang(frameOfReference + 4,rand()%4-1,0.2, COLOR_BLACK)) ;
            }

            if(rand() % 350 == 0)
            {
                    magnet.push_back(Magnet(frameOfReference + 2, rand()%4 - 2 , COLOR_SILVER)) ;
                    magnet_count++ ; 
                    magnet_timer = 120 ; 
            }

            if(rand() % 200 == 0 && dragon_exists == true )
            {
                    iceball.push_back(Iceball(dragon.return_x(), dragon.return_y() , 0.2, COLOR_ICEBALL)) ; 
            }

            if(magnet_timer > 0){
                magnet_timer-- ;
            }
            else if(magnet_count > 0 )
            {
                magnet.erase(magnet.begin()) ; 
                magnet_count-- ; 
            }

            if(in_highway == false){

                coin_collision() ; 

                fline_collision(); 

                fbeam_collision(); 

                powerup_collision() ; 

                exhaust_reaper() ; 

                waterballoon_collision() ; 

                boomerang_collision() ; 

                iceball_collision() ; 
                
                tick_input(window);
            }
            else{
                highway_path() ; 
            }



            reset_screen() ; 
            
            // cout<<"SCORE : "<<score+frameOfReference<<'\n' ;  
        }

        // Poll for Keyboard and mouse events
        glfwPollEvents();
    }

    quit(window);
}

bool detect_collision(bounding_box_t a, bounding_box_t b) {
    return (abs(a.x - b.x) * 2 < (a.width + b.width)) &&
           (abs(a.y - b.y) * 2 < (a.height + b.height));
}

void reset_screen() {
    float top    = screen_center_y + 4 / screen_zoom;
    float bottom = screen_center_y - 4 / screen_zoom;
    float left   = screen_center_x - 4 / screen_zoom;
    float right  = screen_center_x + 4 / screen_zoom;
    Matrices.projection = glm::ortho(left, right, bottom, top, 0.1f, 500.0f);
}

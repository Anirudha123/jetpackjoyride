#include "main.h"

#ifndef POWERUP_H
#define POWERUP_H


class Powerup {
public:
    Powerup() {}
    Powerup(float x, float y, float s,  color_t color, int type);
    glm::vec3 position;
    float rotation;
    void draw(glm::mat4 VP);
    void set_position(float x, float y); 
    void tick();
    float return_x() ; 
    float return_y() ; 
    int return_type() ; 
    void collected() ;
    bounding_box_t get_boundingbox() ; 
private:  
    VAO *object;
    float radius ; 
    float speed ; 
    float yspeed ;
    float xspeed ; 
    int type ;  
    bool up ; 
};

#endif //   Ground_H

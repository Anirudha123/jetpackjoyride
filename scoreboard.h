#include "main.h"

#ifndef SCOREBOARD_H
#define SCOREBOARD_H


class Scoreboard {
public:
    Scoreboard() {}
    Scoreboard(float x, float y, color_t color);
    glm::vec3 position;
    float rotation;
    void draw(glm::mat4 VP, int score);
    void set_position(float x, float y);
    void set_speed(float s) ; 
    void set_yvelocity(float yv) ; 
    void set_x(float x) ;
    void tick();
    float return_x() ; 
    float return_y() ; 
    bool return_raystate() ; 
    bounding_box_t get_boundingbox(); 
private:
    double speed;
    // double gravity ;
    float radius ; 
    bool up ;
    bool ray_state ;  
    int timer ; 
    double Yvelocity ;  
    VAO *object1;
    VAO *object2;
    VAO *object3;
    VAO *object4;
    VAO *object5;
    VAO *object6;
    VAO *object7;

};

#endif // BALL_H

#include "main.h"

#ifndef SEMICIRCULARRING_H
#define SEMICIRCULARRING_H


class Highway {
public:
    Highway() {}
    Highway(float x, float y, float r, color_t color);
    glm::vec3 position;
    float rotation;
    void draw(glm::mat4 VP);
    void set_position(float x, float y); 
    void tick();
    float return_x() ; 
    float return_y() ; 
    void collected() ;
    int return_type() ; 
    float return_radius() ; 
    bounding_box_t get_boundingbox() ; 
private:  
    VAO *object;
    VAO *object2 ; 
    float radius ; 
    float speed ; 
    int type ; 
    color_t color ; 
};

#endif //   Ground_H

#include "main.h"

#ifndef THRUSTER_H
#define THRUSTER_H


class Thruster {
public:
    Thruster() {}
    Thruster(float x, float y, float s,  color_t color);
    glm::vec3 position;
    float rotation;
    void draw(glm::mat4 VP);
    void set_position(float x, float y); 
    void tick();
    float return_x() ; 
    float return_y() ; 
    bounding_box_t get_boundingbox() ; 
private:  
    VAO *object;
    float radius ; 
    float speed ; 
};

#endif //   Thruster_H

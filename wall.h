#include "main.h"

#ifndef WALL_H
#define WALL_H


class Wall {
public:
    Wall() {}
    Wall(float x, float y,  color_t color);
    glm::vec3 position;
    float rotation;
    void draw(glm::mat4 VP);
    void set_position(float x, float y);
    void set_speed(float s) ; 
    void tick();
    float return_x() ; 
    float return_y() ; 
private:  
    VAO *object;
};

#endif //   Ground_H

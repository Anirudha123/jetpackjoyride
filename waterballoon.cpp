#include "main.h"
#include "waterballoon.h"

Waterballoon::Waterballoon(float x, float y, float s,  color_t color) {
    this->position = glm::vec3(x, y, 0);
    this->rotation = 0;
    this->speed = s ; 
    this->yspeed = 0 ; 
    // Our vertices. Three consecutive floats give a 3D vertex; Three consecutive vertices give a triangle.
    // A cube has 6 faces with 2 triangles each, so this makes 6*2=12 triangles, and 12*3 vertices
    static GLfloat vertex_buffer_data[190] ; 
    int numberOfSides = 12 ; 
    this->radius = 0.2 ; 
    for(int i = 0 ; i < numberOfSides ; i++)
		{
				vertex_buffer_data[9*i] = 0.2f ; 
				vertex_buffer_data[9*i+1] = -0.2f ; 
				vertex_buffer_data[9*i+2] = 0.0f ; 
				vertex_buffer_data[9*i+3] = radius * cos(2*M_PI*(i)/numberOfSides)+0.2 ; 
				vertex_buffer_data[9*i+4] = radius * sin(2*M_PI*(i)/numberOfSides) -0.2;
				vertex_buffer_data[9*i+5] = 0.0f ;
				vertex_buffer_data[9*i+6] = radius * cos(2*M_PI*(i+1)/numberOfSides) +0.2; 
				vertex_buffer_data[9*i+7] = radius * sin(2*M_PI*(i+1)/numberOfSides) - 0.2;
				vertex_buffer_data[9*i+8] = 0.0f ;
	    }

    this->object = create3DObject(GL_TRIANGLES, numberOfSides*3, vertex_buffer_data, color, GL_FILL);
}

void Waterballoon::draw(glm::mat4 VP) {
    Matrices.model = glm::mat4(1.0f);
    glm::mat4 translate = glm::translate (this->position);    // glTranslatef
    glm::mat4 rotate    = glm::rotate((float) (this->rotation * M_PI / 180.0f), glm::vec3(1, 0, 0));
    // No need as coords centered at 0, 0, 0 of cube arouund which we waant to rotate
    // rotate          = rotate * glm::translate(glm::vec3(0, -0.6, 0));
    Matrices.model *= (translate * rotate);
    glm::mat4 MVP = VP * Matrices.model;
    glUniformMatrix4fv(Matrices.MatrixID, 1, GL_FALSE, &MVP[0][0]);
    draw3DObject(this->object);
}

void Waterballoon::set_position(float x, float y) {
    this->position = glm::vec3(x, y, 0);
}

float Waterballoon::return_x(){
    return this->position.x ; 
}

float Waterballoon::return_y(){
    return this->position.y ; 
}


bounding_box_t Waterballoon::get_boundingbox(){
    bounding_box_t b ;
    b.x = this->position.x ; 
    b.y = this->position.y ; 
    b.height =  2 * this->radius  ; 
    b.width = 2 * this->radius ; 

    return b ; 
}


void Waterballoon::tick() {

    this->position.y += this->yspeed ;
    this->position.x += this->speed ;  
    this->yspeed -= 0.005;
    return ; 
}

